
class Parent{

	int x = 10 ;
	String str1 = "Viv";

	void ParentMethod(){

		System.out.println(x);
		System.out.println(str1);
	}
}

class Child extends Parent {

	int x = 20 ;
	String str1 = "Ajay";

	void ChildMethod(){

		System.out.println(x);
		System.out.println(str1);
	}
}
class Client {
	public static void main(String[] args){

	Child obj = new Child();

	System.out.println(obj.x);  
	System.out.println(obj.str1);
		
	obj.ParentMethod();

	System.out.println(obj.x);
	System.out.println(obj.str1);

	obj.ChildMethod(); 
	}
}

