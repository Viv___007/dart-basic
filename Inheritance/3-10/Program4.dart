
class Parent{

	int x = 10 ;
	
	Parent(){

		print(x);
		print(this.hashCode);

	}
	
	void parentData(){

		print(x);
	}
}

class Child extends Parent{
	
	int x = 20 ;

	Child(){
		
		print("In Child Constrctor");
		print(this.hashCode);
	}

	void dispData(){

		print(x);
	}
}

void main(){

	Child obj = new Child();
}
