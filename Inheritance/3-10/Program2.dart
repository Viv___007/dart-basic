
class Parent{

	int x = 10 ;

	String str1 ="name";

	int ParentMethod(){

		print(x);
		print(str1);
		
		return 0 ;
	}
	
}

class Child extends Parent{

	int y = 20 ;

	String str2 = "data";

	int ChildMethod(){

		print(y);
		print(str2);
		
		return 0 ;
	}
}

void main(){

	Parent obj1 = new Parent();
	
	print(obj1.x);
	print(obj1.str1);
	print(obj1.ParentMethod());

	Child obj2 = new Child();
	
	print(obj2.x);
	print(obj2.str1);
	print(obj2.ChildMethod());
}
	
