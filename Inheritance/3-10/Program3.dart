
class Parent{

	int x = 10 ;
	String str1 = "Viv";

	void ParentMethod(){

		print(x);
		print(str1);
	}
}

class Child extends Parent {

	int x = 20 ;
	String str1 = "Ajay";

	void ChildMethod(){

		print(x);
		print(str1);
	}
}
	void main(){

	Child obj = new Child();

	print(obj.x);
	print(obj.str1);
	
	obj.ParentMethod();

	print(obj.x);
	print(obj.str1);

	obj.ChildMethod(); 
}

