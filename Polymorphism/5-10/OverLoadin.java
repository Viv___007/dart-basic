
class Demo {

	int x = 10 , y = 20 ;

	void disp(int x){
		
		this.x = x ;
		
		System.out.println(x);

	}
	
	void disp(int x, int y){
		
		this.x = x ;
		this.y = y ;
	
		System.out.println(x);
		System.out.println(y);
		
	}
}
class Client{
	
	public static void main(String[] args){

	Demo obj = new Demo();

	obj.disp(10);
	obj.disp(10,20);
	}
}

	
