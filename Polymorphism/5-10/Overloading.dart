
class Demo {

	int x = 10 , y = 20 ;

	void disp(int x){
		
		this.x = x ;
		print(x);

	}
	
	void disp(int x, int y){
		
		this.x = x ;
		print(x);
		print(y);
	}
}

void main(){

	Demo obj = new Demo();

	obj.disp(10);
	obj.disp(10,20);
}

	
