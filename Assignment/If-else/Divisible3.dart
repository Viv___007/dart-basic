//Write adart program to check whether the number is divisible by 3 & 5 ie If the number is divisible by both 3 and 5 : 0/p "Divisible by 5"
	
void main(){
	
	int Input = 34 ;

	if( Input % 3 == 0 || Input % 5 == 0 ){

		print("Divisible by  both ");

	}else if( Input % 3 == 0 && Input % 5 != 0 ){

		print("Divisible by 3");
	
	}else if( Input % 3 != 0 && Input % 5 == 0 ){
	
		print("Divisible by 5 ");
	
	}else if( Input % 3 != 0 && Input % 5 != 0 ){
	
		print("Not Divisible by Both");

	}else {

		print("INvalid Input");

	}
}
