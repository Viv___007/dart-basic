//Write a program to print the sum of all even number and multiplication of odd number bet 1 t0 10.

void main(){
	int sum = 0 , mul = 1;
	int i = 1;
	while( i <= 10){

		if(i%2 == 0){
			sum =sum + i;
		}else{

			mul = mul*i;	
		}
		i++;
	}
	print("sum of even number between 1 to 10 : $sum");
	print("Multiplication of even number between 1 to 10 : $mul");
}
