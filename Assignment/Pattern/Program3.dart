/*

14 14 14 14
15 15 15 15
16 16 16 16
17 17 17 17  

*/

import 'dart:io';

void main(){

	int row = 4 ;
	
	int number = 14 ;

	for(int i = 1 ; i <= row ; i++){
		
		for( int j = 1 ; j <= row ; j++ ){
		
			stdout.write(" $number ");
		}
		print(" ");
		number++;
	}
}
