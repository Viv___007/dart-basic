/*

1  3  5
7  9  11
13 15 17  

*/

import 'dart:io';

void main(){

	int row = 3 , val = 1 ;
	
	for(int i = 1 ; i <= row ; i++){
		
		for( int j = 1 ; j <= row ; j++ ){

			stdout.write(" $val ");
			val = val + 2 ;
		}
		print(" ");
	}
}
