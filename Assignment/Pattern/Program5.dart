/*

12 12 12
11 11 11
10 10 10 

*/

import 'dart:io';

void main(){

	int row = 3 ;
	
	int number = 12 ;

	for(int i = 1 ; i <= row ; i++){
		
		for( int j = 1 ; j <= row ; j++ ){
		
			stdout.write(" $number ");
		}
		print(" ");
		number--;
	}
}
