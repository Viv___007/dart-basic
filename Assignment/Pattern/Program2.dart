/*

4  5  6  7  
4  5  6  7  
4  5  6  7  
4  5  6  7  

*/

import 'dart:io';

void main(){

	int row = 4 ;

	for(int i = 1 ; i <= row ; i++){
		int temp = row ;
		for( int j = 1 ; j <= row ; j++ ){

			stdout.write(" $temp ");
			temp++;	
		}
		print(" ");
	}
}
