import 'GetterProgram1.dart';

void main(){
	
	Demo obj1 = new Demo(10 , "Viv" , 9.0);

	obj1.disp();

	obj1.setX(15);
	obj1.setName("Rahul");
	obj1.setSal(2.0);

	obj1.disp();
	
	obj1.setX = 15;
	obj1.setName = "Rahul";
	obj1.setSal = 2.0;
	
	obj1.disp();
}
