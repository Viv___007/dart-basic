class Demo{

	int x = 10 ;

	static int y = 20;

	void printData(){

		print(x);
		print(y);
	}

}

void main(){

	Demo obj1 = new Demo();
	obj1.printData();
	
	print("----------------------------------");
	Demo obj2 = new Demo();
	
	obj1.x = 50 ;

	obj1.printData();
	obj2.printData();
}
